﻿using System;
using System.Diagnostics;
using System.Drawing;
using AutoCV.Extensions;
using AutoCV.Helpers;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using Point = System.Drawing.Point;

namespace AutoCV
{
    // ReSharper disable once InconsistentNaming
    public class AutoCV : Vision
    {
        private readonly ImageViewer.ImageViewer _imgViewer;
        private readonly Input _io;
        private readonly Random _random;

        public AutoCV()
        {
            _imgViewer = new ImageViewer.ImageViewer();
            _io = new Input();
            _random = new Random();
        }

        public Point CursorPosition { get; set; } = Point.Empty;
        public int MouseSpeed { get; set; } = 20;

        /// <summary>
        ///     Shows what's currently saved in the backbuffer
        /// </summary>
        public void ShowBackbuffer()
        {
            _imgViewer.SetImage(Screenshot);
        }

        public void ShowImage(Bitmap img)
        {
            _imgViewer.SetImage(img);
        }

        /// <summary>
        ///     Draws a rect onto the backbuffer
        /// </summary>
        /// <param name="rect">the area rectangle to draw</param>
        public void DrawRect(Rect rect)
        {
            using var reference = ScreenshotMat.Clone();
            Cv2.Rectangle(reference, rect, Scalar.LimeGreen, 2);
            Screenshot = reference.ToBitmap();
        }

        public void ClickMouse(Point destination)
        {
            MoveMouse(destination);
            _io.SilentClick(Handle, destination);
        }

        public void MoveMouse(Point destination)
        {
            var randomSpeed = (_random.Next(MouseSpeed) / 2d + MouseSpeed) / 10d;
            HumanMoveMouse(destination.X, destination.Y, 7, 5, 10d * randomSpeed);
        }

        private void HumanMoveMouse(double xDestination, double yDestination, double gravity, double wind,
            double targetArea)
        {
            double xStart = CursorPosition.X, yStart = CursorPosition.Y;
            double velocityX = 0, velocityY = 0, windX = 0d, windY = 0d;

            var sqrt2 = Math.Sqrt(2);
            var sqrt3 = Math.Sqrt(3);
            var sqrt5 = Math.Sqrt(5);

            var totalDistance = Utils.Distance(Math.Round(xStart), Math.Round(yStart), Math.Round(xDestination),
                Math.Round(yDestination)).ToRoundedInt();
            Stopwatch timer = new();

            var dist = Utils.Distance(xStart - xDestination, yStart - yDestination);
            while (dist >= 1)
            {
                if (timer.Elapsed.Seconds > 10)
                    break;

                wind = Math.Min(wind, dist);
                dist = Math.Max(dist, 1);

                var d = (Math.Round((double)totalDistance) * 0.3 / 7).ToRoundedInt();
                d = d switch
                {
                    > 25 => 25,
                    < 5 => 5,
                    _ => d
                };

                var rCnc = _random.Next(6);
                if (rCnc == 1) d = _random.Next(2, 3);

                var maxStep = d <= Math.Round(dist) ? d : Math.Round(dist);

                if (dist >= targetArea)
                {
                    windX = windX / sqrt3 + (_random.Next(wind.ToRoundedInt() * 2 + 1) - wind) / sqrt5;
                    windY = windY / sqrt3 + (_random.Next(wind.ToRoundedInt() * 2 + 1) - wind) / sqrt5;
                }
                else
                {
                    windX /= sqrt2;
                    windY /= sqrt2;
                }

                velocityX += windX + gravity * (xDestination - xStart) / dist;
                velocityY += windY + gravity * (yDestination - yStart) / dist;

                if (Utils.Distance(velocityX, velocityY) > maxStep)
                {
                    var randomDist = maxStep / 2.0 + _random.Next(maxStep.ToRoundedInt() % 2);
                    var veloMag = Utils.Distance(velocityX, velocityY);
                    velocityX = velocityX / veloMag * randomDist;
                    velocityY = velocityY / veloMag * randomDist;
                }

                var lastX = xStart.ToRoundedInt();
                var lastY = yStart.ToRoundedInt();
                xStart += velocityX;
                yStart += velocityY;

                if (lastX != xStart.ToRoundedInt() || lastY != yStart.ToRoundedInt())
                {
                    CursorPosition = new Point(xStart.ToRoundedInt(), yStart.ToRoundedInt());
                    _io.SilentMoveMouseStep(Handle, CursorPosition);
                }

                var w = _random.Next(600 / MouseSpeed);
                if (w < 5) w = 5;
                w = (w * 0.9).ToRoundedInt();
                Utils.Sleep(w);

                dist = Utils.Distance(xStart - xDestination, yStart - yDestination);
            }

            if (xDestination.ToRoundedInt() == xStart.ToRoundedInt() &&
                xDestination.ToRoundedInt() == yStart.ToRoundedInt()) return;

            CursorPosition = new Point(xStart.ToRoundedInt(), yStart.ToRoundedInt());
            _io.SilentMoveMouseStep(Handle, CursorPosition);
        }
    }
}