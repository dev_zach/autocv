﻿using System.Drawing;
using OpenCvSharp;
using Size = OpenCvSharp.Size;

namespace AutoCV
{
    public static class Converters
    {
        /// <summary>
        ///     Converts an System.Drawing.Rectangle to an OpenCvSharp.Rect
        /// </summary>
        /// <param name="rect">The System.Drawing.Rectangle to convert</param>
        /// <returns>The converted rectangle</returns>
        public static Rect ToOpenCvRect(this Rectangle rect)
        {
            return new Rect(rect.X, rect.Y, rect.Width, rect.Height);
        }

        /// <summary>
        ///     Converts an instance of System.Drawing.Size to OpenCv.Size
        /// </summary>
        /// <param name="size">the System.Drawing.Size to convert</param>
        /// <returns>the converted size</returns>
        public static Size ToOpenCvSize(this System.Drawing.Size size)
        {
            return new Size(size.Width, size.Height);
        }

        /// <summary>
        ///     Converts a Tesseract.Rect to an OpenCvSharp.Rect
        /// </summary>
        /// <param name="rect">The Tesseract.Rect to convert</param>
        /// <returns>The converted rectangle</returns>
        public static Rect ToOpenCvRect(this Tesseract.Rect rect)
        {
            return new Rect(rect.X1, rect.Y1, rect.Width, rect.Height);
        }

        public static Scalar ToScalar(this Color color, int tolerance = 0, bool add = true)
        {
            return add
                ? Scalar.FromRgb(color.R + tolerance, color.G + tolerance, color.B + tolerance)
                : Scalar.FromRgb(color.R - tolerance, color.G - tolerance, color.B - tolerance);
        }
    }
}