﻿using System;

namespace AutoCV.CustomExceptions
{
    public class WindowNotFoundException : Exception
    {
        public WindowNotFoundException()
        {
        }

        public WindowNotFoundException(string message)
            : base(message)
        {
        }

        public WindowNotFoundException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}