﻿using System;

namespace AutoCV.CustomExceptions
{
    public class WindowNotSetException : Exception
    {
        public WindowNotSetException()
        {
        }

        public WindowNotSetException(string message)
            : base(message)
        {
        }

        public WindowNotSetException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}