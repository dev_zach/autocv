﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace AutoCV.Extensions
{
    public static class BitmapExtensions
    {
        /// <summary>
        ///     converts a bitmap to a string
        /// </summary>
        /// <param name="image">the image to convert</param>
        /// <returns>a compressed string that represents the bitmap</returns>
        public static string ToString(this Bitmap image)
        {
            using var ms = new MemoryStream();
            image.Save(ms, ImageFormat.Bmp);
            byte[] imageBytes = ms.ToArray();
            var bitmapString = Convert.ToBase64String(imageBytes);
            return bitmapString.Compress();
        }
    }
}