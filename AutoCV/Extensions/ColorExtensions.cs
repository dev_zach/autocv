﻿using System.Drawing;

namespace AutoCV.Extensions
{
    public static class ColorExtensions
    {
        /// <summary>
        ///     inverts the current color
        /// </summary>
        /// <param name="c">color to invert</param>
        /// <returns>the inverted color</returns>
        public static Color InvertColor(this Color c)
        {
            var red = 255 - c.R;
            var green = 255 - c.G;
            var blue = 255 - c.B;

            return Color.FromArgb(c.A, red, green, blue);
        }

        /// <summary>
        ///     converts a color to a hex string
        /// </summary>
        /// <param name="c">the color to convert</param>
        /// <returns>the string hex value</returns>
        public static string ToHex(this Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        public static int ToInt(this Color c)
        {
            return (c.R << 16) + (c.G << 8) + c.B;
        }
    }
}