﻿using System;

namespace AutoCV.Extensions
{
    public static class NumberExtensions
    {
        public static int ToRoundedInt(this double num)
        {
            return (int)Math.Round(num);
        }
    }
}