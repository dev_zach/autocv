﻿using System;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace AutoCV.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        ///     compares 2 strings while ignoring case.
        /// </summary>
        /// <param name="source">the source string to compare with</param>
        /// <param name="value">the value string to compare the source to</param>
        /// <returns>returns true if the strings match else false</returns>
        public static bool ContainsIgnoreCase(this string source, string value)
        {
            return source?.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        /// <summary>
        ///     decompresses a string.
        /// </summary>
        /// <param name="compressedString">the string to decompress</param>
        /// <returns>a decompressed string</returns>
        public static string Decompress(this string compressedString)
        {
            using var compressedStream = new MemoryStream(Convert.FromBase64String(compressedString));
            using var decompressorStream = new DeflateStream(compressedStream, CompressionMode.Decompress);
            using var decompressedStream = new MemoryStream();

            decompressorStream.CopyTo(decompressedStream);
            var decompressedBytes = decompressedStream.ToArray();

            return Encoding.UTF8.GetString(decompressedBytes);
        }

        /// <summary>
        ///     converts a string into a bitmap
        /// </summary>
        /// <param name="compressedString">string to convert</param>
        /// <returns>a bitmap converted from the string</returns>
        public static Bitmap ToBitmap(string compressedString)
        {
            byte[] imageBytes = Convert.FromBase64String(compressedString.Decompress());
            using var ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            return new Bitmap(ms, true);
        }

        /// <summary>
        ///     compresses a string
        /// </summary>
        /// <param name="uncompressedString">The string to compress</param>
        /// <returns>A compressed string</returns>
        public static string Compress(this string uncompressedString)
        {
            using var uncompressedStream = new MemoryStream(Encoding.UTF8.GetBytes(uncompressedString));
            using var compressedStream = new MemoryStream();
            using var compressorStream = new DeflateStream(compressedStream, CompressionLevel.Fastest, true);

            uncompressedStream.CopyTo(compressorStream);
            byte[] compressedBytes = compressedStream.ToArray();

            return Convert.ToBase64String(compressedBytes);
        }
    }
}