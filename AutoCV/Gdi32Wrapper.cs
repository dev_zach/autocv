﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace AutoCV
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Gdi32Wrapper
    {
        public const int SRCCOPY = 0x00CC0020; // BitBlt dwRop parameter

        public static bool BitBlt(
            IntPtr hObject, int nXDest, int nYDest, int nWidth, int nHeight,
            IntPtr hObjectSource, int nXSrc, int nYSrc, int dwRop
        )
        {
            return Gdi32.BitBlt(hObject, nXDest, nYDest, nWidth, nHeight, hObjectSource, nXSrc, nYSrc, dwRop);
        }

        public static IntPtr CreateCompatibleBitmap(IntPtr hDC, int nWidth, int nHeight)
        {
            return Gdi32.CreateCompatibleBitmap(hDC, nWidth, nHeight);
        }

        public static IntPtr CreateCompatibleDC(IntPtr hDC)
        {
            return Gdi32.CreateCompatibleDC(hDC);
        }

        public static bool DeleteDC(IntPtr hDC)
        {
            return Gdi32.DeleteDC(hDC);
        }

        public static bool DeleteObject(IntPtr hObject)
        {
            return Gdi32.DeleteObject(hObject);
        }

        public static IntPtr SelectObject(IntPtr hDC, IntPtr hObject)
        {
            return Gdi32.SelectObject(hDC, hObject);
        }
    }
}