﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using OpenCvSharp.Extensions;

namespace AutoCV.Helpers
{
    public static class Utils
    {
        /// <summary>
        ///     Sleeps the current thread until the timeout is met
        /// </summary>
        /// <param name="millisecondsTimeout">milliseconds to time out for</param>
        public static void Sleep(int millisecondsTimeout)
        {
            Thread.Sleep(millisecondsTimeout);
        }

        /// <summary>
        ///     Calculates the distance between 2 points
        /// </summary>
        /// <param name="start">start point</param>
        /// <param name="end">end point</param>
        /// <returns>the distance between the 2 points</returns>
        public static double Distance(Point start, Point end)
        {
            return Math.Sqrt(Math.Pow(end.X - start.X, 2) + Math.Pow(end.Y - start.Y, 2));
        }

        /// <summary>
        ///     Calculates the distance between 2 points
        /// </summary>
        /// <param name="xStart">x starting point</param>
        /// <param name="yStart">y starting point</param>
        /// <param name="xEnd">x ending point</param>
        /// <param name="yEnd">y ending point</param>
        /// <returns></returns>
        public static double Distance(double xStart, double yStart, double xEnd, double yEnd)
        {
            return Math.Sqrt(Math.Pow(xEnd - xStart, 2) + Math.Pow(yEnd - yStart, 2));
        }

        public static double Distance(double start, double end)
        {
            return Math.Sqrt(Math.Pow(start, 2) + Math.Pow(end, 2));
        }

        /// <summary>
        ///     Returns the sub area of a bitmap
        /// </summary>
        /// <param name="source">the bitmap</param>
        /// <param name="region">the area to cut</param>
        /// <returns>the bitmap portion</returns>
        public static Bitmap GetBitmapRegion(Bitmap source, Rectangle region)
        {
            return source.ToMat()[region.ToOpenCvRect()].ToBitmap();
        }

        public static Color AverageColor(IEnumerable<Color> colors)
        {
            var colorsList = colors.ToList();
            var r = (int)colorsList.Average(c => c.R);
            var g = (int)colorsList.Average(c => c.G);
            var b = (int)colorsList.Average(c => c.B);
            return Color.FromArgb(r, g, b);
        }

        public static Bitmap RegularScreenshot()
        {
            Screen currentMonitor = Screen.FromPoint(Cursor.Position);
            var bmpScreen = new Bitmap(currentMonitor.Bounds.Width, currentMonitor.Bounds.Height,
                PixelFormat.Format32bppArgb);
            var screenGfx = Graphics.FromImage(bmpScreen);
            screenGfx.CopyFromScreen(currentMonitor.Bounds.X, currentMonitor.Bounds.Y, 0, 0, currentMonitor.Bounds.Size,
                CopyPixelOperation.SourceCopy);

            return bmpScreen;
        }

        /// <summary>
        ///     Given a list of colors, this will return the color that returns the smallest tolerance to include all
        ///     other colors in the list.
        /// </summary>
        /// <param name="colors"></param>
        /// <returns></returns>
        public static (Color, (int, int, int)) FindOptimalColorWithTolerance(IEnumerable<Color> colors)
        {
            var colorsList = colors.ToList();
            var averageColor = AverageColor(colorsList);

            var r = colorsList.Select(c => Math.Abs(averageColor.R - c.R)).Max();
            var g = colorsList.Select(c => Math.Abs(averageColor.G - c.G)).Max();
            var b = colorsList.Select(c => Math.Abs(averageColor.B - c.B)).Max();

            return (averageColor, (r, g, b));
        }

        public static Bitmap? GetPixelGrid(IntPtr hwnd, Point searchPoint, int gridWidth, int gridHeight)
        {
            var grid = new Bitmap(gridWidth, gridHeight, PixelFormat.Format32bppArgb);
            using Graphics dest = Graphics.FromImage(grid);
            using Graphics src = Graphics.FromHwnd(hwnd);
            var srcDc = src.GetHdc();
            var destDc = dest.GetHdc();
            var success = Gdi32.BitBlt(destDc, 0, 0, gridWidth, gridHeight, srcDc, searchPoint.X - gridWidth / 2,
                searchPoint.Y - gridHeight / 2, (int)CopyPixelOperation.SourceCopy);
            dest.ReleaseHdc();
            src.ReleaseHdc();

            return success ? grid : null;
        }
    }
}