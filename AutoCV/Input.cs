﻿using System;
using System.Drawing;
using System.Threading;

namespace AutoCV
{
    public class Input
    {
        private readonly Random _random = new();

        /// <summary>
        ///     Figures out how to sleep between key presses, low being low range, high being high range. If both are present, do
        ///     random value between the two.
        /// </summary>
        /// <param name="low">Low value in range.</param>
        /// <param name="high">High value in range.</param>
        private void DoKeySleep(int? low, int? high)
        {
            var useRand = low != null && high != null;
            var exact = low ?? high ?? null;
            if (useRand || exact != null)
                Thread.Sleep(useRand
                    ? _random.Next(low.GetValueOrDefault(), high.GetValueOrDefault() + 1)
                    : exact.GetValueOrDefault());
        }

        /// <summary>
        ///     Types silently (no need for window focus). To not use human type, do not set humanTypeLow or humanTypeHigh (or make
        ///     them null). If you want an exact value to sleep between key presses, only set one of these values.
        /// </summary>
        /// <param name="hwnd">Window handle</param>
        /// <param name="text">Text to send</param>
        /// <param name="sendEnter">Whether to send the enter key afterwards.</param>
        /// <param name="humanTypeLow">Human type sleep range low.</param>
        /// <param name="humanTypeHigh">Human type sleep range high.</param>
        public void SilentType(IntPtr hwnd, string text, bool sendEnter, int? humanTypeLow = null,
            int? humanTypeHigh = null)
        {
            foreach (var c in text)
            {
                var vk = Win32Wrapper.VkKeyScan(c);
                Win32Wrapper.SendMessageA(hwnd, (uint)Win32Wrapper.Constants.WM_KEYDOWN, vk, 0x00250001);
                Win32Wrapper.SendMessageA(hwnd, (uint)Win32Wrapper.Constants.WM_CHAR, c, 0x00250001);
                Thread.Sleep(5);
                Win32Wrapper.SendMessageA(hwnd, (uint)Win32Wrapper.Constants.WM_KEYUP, vk, 0xC0250001);
                DoKeySleep(humanTypeLow, humanTypeHigh);
            }

            if (sendEnter)
            {
                Win32Wrapper.SendMessageA(hwnd, (uint)Win32Wrapper.Constants.WM_KEYDOWN,
                    (int)Win32Wrapper.Constants.VK_RETURN,
                    0x00250001);
                Win32Wrapper.SendMessageA(hwnd, (uint)Win32Wrapper.Constants.WM_CHAR, (char)13, 0x00250001);
                Thread.Sleep(5);
                Win32Wrapper.SendMessageA(hwnd, (uint)Win32Wrapper.Constants.WM_KEYUP,
                    (int)Win32Wrapper.Constants.VK_RETURN,
                    0xC0250001);
            }
        }

        /// <summary>
        ///     Imitates mouse move to coordinate pt. This is not a full function. To mimick human mouse movement, this function
        ///     needs to be called many times throughout the movement. This func is only one "step" in the movement from pt A to pt
        ///     B.
        /// </summary>
        /// <param name="hwnd">Window handle to send message to.</param>
        /// <param name="pt">Point to move mouse to.</param>
        /// <param name="screenPt">True if point needs to be converted to hwnd's client space.</param>
        public void SilentMoveMouseStep(IntPtr hwnd, Point pt, bool screenPt = false)
        {
            Win32Wrapper.POINT clientPoint = pt;
            if (screenPt) Win32Wrapper.ScreenToClient(hwnd, ref clientPoint);
            else clientPoint = pt;

            var lparam = (uint)Win32Wrapper.MAKELPARAM(clientPoint.X, clientPoint.Y);
            Thread.Sleep(3);
            Win32Wrapper.SendMessageA(hwnd, (uint)Win32Wrapper.Constants.WM_MOUSEMOVE, 0, lparam);
        }

        /// <summary>
        ///     Imitates click on the specified window, at pt.
        /// </summary>
        /// <param name="hwnd">Window handle to send message to.</param>
        /// <param name="pt">Point to click on.</param>
        /// <param name="screenPt">True if pt needs to be converted to hwnd's client space.</param>
        /// <param name="clickSleep">
        ///     Set to 0 for no sleep, otherwise this is the sleep time in ms between mouse button down and
        ///     mouse button up.
        /// </param>
        /// <param name="mouseButton">Which mouse button to click with. Left, middle, right.</param>
        public void SilentClick(IntPtr hwnd, Point pt, bool screenPt = true, int clickSleep = 20,
            Win32Wrapper.Constants mouseButton = Win32Wrapper.Constants.MK_LBUTTON)
        {
            Win32Wrapper.POINT clientPoint = pt;
            if (screenPt) Win32Wrapper.ScreenToClient(hwnd, ref clientPoint);
            else clientPoint = pt;

            var lparam = (uint)Win32Wrapper.MAKELPARAM(clientPoint.X, clientPoint.Y);

            Win32Wrapper.SendMessageA(hwnd, (uint)Win32Wrapper.Constants.WM_LBUTTONDOWN, (int)mouseButton, lparam);
            if (clickSleep > 0) Thread.Sleep(clickSleep);
            Win32Wrapper.SendMessageA(hwnd, (uint)Win32Wrapper.Constants.WM_LBUTTONUP, (int)mouseButton, lparam);
        }
    }
}