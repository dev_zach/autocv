﻿using Tesseract;

namespace AutoCV.Models
{
    public readonly struct FoundTextWithRectAndConfidence
    {
        public Rect ComponentRect { get; }
        public string ComponentText { get; }
        public float Confidence { get; }

        public FoundTextWithRectAndConfidence(Rect componentRect, string componentText, float confidence)
        {
            ComponentRect = componentRect;
            ComponentText = componentText;
            Confidence = confidence;
        }

        public override string ToString()
        {
            return $@"{{{ComponentRect.ToString()}, FoundText={ComponentText}, Confidence={Confidence}}}";
        }
    }
}