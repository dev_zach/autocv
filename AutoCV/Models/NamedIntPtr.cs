﻿using System;

namespace AutoCV.Models
{
    public readonly struct NamedIntPtr
    {
        public IntPtr Ptr { get; }
        public string Name { get; }

        public NamedIntPtr(IntPtr ptr, string name)
        {
            Ptr = ptr;
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }
    }
}