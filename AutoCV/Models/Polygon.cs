﻿using System.Collections.Generic;
using System.Linq;
using OpenCvSharp;

namespace AutoCV.Models
{
    public struct Polygon
    {
        public readonly List<Point> Vertices;

        public Polygon(IEnumerable<Point> vertices)
        {
            Vertices = vertices.ToList();
        }

        /// <summary>
        ///     Checks if the given point lies within the polygon
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool Contains(Point point)
        {
            return Cv2.PointPolygonTest(Vertices, point, false) == 0f;
        }

        public Point Centroid()
        {
            var moments = Cv2.Moments(Vertices);
            return new Point(moments.M10 / moments.M00, moments.M01 / moments.M00);
        }
    }
}