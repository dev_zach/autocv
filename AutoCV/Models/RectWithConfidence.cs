﻿using OpenCvSharp;

namespace AutoCV.Models
{
    public readonly struct RectWithConfidence
    {
        public Rect Rect { get; }
        public double Confidence { get; }

        public RectWithConfidence(Rect rect, double confidence)
        {
            Rect = rect;
            Confidence = confidence;
        }

        public override string ToString()
        {
            return $"Rect: {Rect.ToString()}, Confidence: {Confidence}";
        }
    }
}