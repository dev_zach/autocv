﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using AutoCV.Models;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using Tesseract;
using Point = System.Drawing.Point;
using Rect = OpenCvSharp.Rect;
using Size = OpenCvSharp.Size;

namespace AutoCV
{
    public class Vision : WindowCapture
    {
        private protected Vision()
        {
            var workingDirectory = Environment.CurrentDirectory;
            var projectDirectory = Directory.GetParent(workingDirectory)?.Parent?.Parent?.Parent?.FullName;
            var dataPath = $@"{projectDirectory}\AutoCV\TessData\";

            Engine = new TesseractEngine(dataPath, "runescape", EngineMode.LstmOnly);
        }

        private TesseractEngine Engine { get; }

        /// <summary>
        ///     Disposes the tesseract engine to prevent memory leaks
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            GC.SuppressFinalize(this);
            Engine.Dispose();
        }

        /// <summary>
        ///     Finds polygons with a certain color
        /// </summary>
        /// <param name="color">the color to search for</param>
        /// <param name="tolerance">the tolerance to search with</param>
        /// <param name="rect">the area to search for polygons</param>
        /// <returns>a list of the found polygons</returns>
        public IEnumerable<Polygon> FindPolygons(Color color, int tolerance = 0, Rect rect = default)
        {
            var part = rect == default ? ScreenshotMat.Clone() : ScreenshotMat.Clone()[rect];

            var lowerRange = color.ToScalar(tolerance);
            var upperRange = color.ToScalar(tolerance, false);

            Cv2.InRange(part, lowerRange, upperRange, part);
            Cv2.Threshold(part, part, 127, 255, ThresholdTypes.Binary);

            // We need 2 iterations of this. The first iteration closes gaps in the polygon edges
            Cv2.FindContours(part, out var contours, out _, RetrievalModes.External,
                ContourApproximationModes.ApproxSimple);

            foreach (var contour in contours)
            {
                var approx = Cv2.ApproxPolyDP(contour, 0.01 * Cv2.ArcLength(contour, true), true);
                Cv2.DrawContours(part, new List<OpenCvSharp.Point[]> { approx }, 0, Scalar.White, 2);
            }

            Cv2.FindContours(part, out var secondContours, out _, RetrievalModes.External,
                ContourApproximationModes.ApproxSimple);

            return secondContours
                .Select(contour => Cv2.ApproxPolyDP(contour, 0.01 * Cv2.ArcLength(contour, true), true))
                .Select(approx => new Polygon(approx)).ToList();
        }

        /// <summary>
        ///     Finds all text within a given area
        /// </summary>
        /// <param name="rect">the area to search</param>
        /// <param name="confidence">the minimum confidence confidence to return</param>
        /// <returns>a list of the found text</returns>
        public IEnumerable<FoundTextWithRectAndConfidence> GetAllText(Rect rect = default, double confidence = .93)
        {
            confidence *= 100;
            const int zoom = 2;
            var foundTextWithLocationAndConfidence = new List<FoundTextWithRectAndConfidence>();

            var part = rect == default ? ScreenshotMat.Clone() : ScreenshotMat.Clone()[rect];
            Cv2.CvtColor(part, part, ColorConversionCodes.BGR2GRAY);
            part = part.Resize(Size.Zero, zoom, zoom, InterpolationFlags.Cubic);

            using var pixImage = PixConverter.ToPix(part.ToBitmap());

            using var page = Engine.Process(pixImage, PageSegMode.SparseText);

            using var iter = page.GetIterator();
            do
            {
                if (!iter.TryGetBoundingBox(PageIteratorLevel.Block, out var rectBounds)) continue;

                var foundConfidence = iter.GetConfidence(PageIteratorLevel.Block);
                if (foundConfidence < confidence) continue;

                var text = iter.GetText(PageIteratorLevel.Block).TrimEnd();
                rectBounds = new Tesseract.Rect(rectBounds.X1 / zoom, rectBounds.Y1 / zoom,
                    rectBounds.Width / zoom,
                    rectBounds.Height / zoom);

                foundTextWithLocationAndConfidence.Add(
                    new FoundTextWithRectAndConfidence(rectBounds, text, foundConfidence / 100));
            } while (iter.Next(PageIteratorLevel.Block));

            return foundTextWithLocationAndConfidence;
        }

        /// <summary>
        ///     Searches for the given text and returns the first found instance
        /// </summary>
        /// <param name="searchText">The text to search for</param>
        /// <param name="rect">The area to search through</param>
        /// <param name="confidence">The confidence to search with</param>
        /// <returns></returns>
        public FoundTextWithRectAndConfidence? FindText(string searchText, Rect rect = default, double confidence = .93)
        {
            confidence *= 100;
            const int zoom = 2;

            var part = rect == default ? ScreenshotMat.Clone() : ScreenshotMat.Clone()[rect];
            Cv2.CvtColor(part, part, ColorConversionCodes.BGR2GRAY);
            part = part.Resize(Size.Zero, zoom, zoom, InterpolationFlags.Cubic);

            using var pixImage = PixConverter.ToPix(part.ToBitmap());

            using var page = Engine.Process(pixImage, PageSegMode.SparseText);

            using var iter = page.GetIterator();
            do
            {
                if (!iter.TryGetBoundingBox(PageIteratorLevel.Block, out var rectBounds)) continue;

                var foundConfidence = iter.GetConfidence(PageIteratorLevel.Block);
                if (foundConfidence < confidence) continue;

                var text = iter.GetText(PageIteratorLevel.Block).TrimEnd();
                if (!text.Equals(searchText)) continue;

                rectBounds = new Tesseract.Rect(rectBounds.X1 / zoom, rectBounds.Y1 / zoom,
                    rectBounds.Width / zoom,
                    rectBounds.Height / zoom);

                return new FoundTextWithRectAndConfidence(rectBounds, text, foundConfidence / 100);
            } while (iter.Next(PageIteratorLevel.Block));

            return null;
        }

        /// <summary>
        ///     Searches for a color
        /// </summary>
        /// <param name="searchColor">the color to search for</param>
        /// <param name="rect">the area to search for</param>
        /// <returns>the list of colors matching the given color</returns>
        public IEnumerable<Point> GetColor(Color searchColor, Rect rect = default)
        {
            var part = rect == default ? ScreenshotMat : ScreenshotMat[rect];
            var typeSpecificMat = new Mat<Vec3b>(part);
            var indexer = typeSpecificMat.GetIndexer();
            var colorVec3B = new Vec3b(searchColor.B, searchColor.G, searchColor.R);

            var points = new List<Point>();
            for (var y = 0; y < part.Height; y++)
            for (var x = 0; x < part.Width; x++)
                if (colorVec3B == indexer[y, x])
                    points.Add(new Point(x, y));

            return points;
        }

        /// <summary>
        ///     Gets the color at the given pixel
        /// </summary>
        /// <param name="searchPoint">the pixel to identify</param>
        /// <returns>the color found</returns>
        public Color GetPixel(Point searchPoint)
        {
            var typeSpecificMat = new Mat<Vec3b>(ScreenshotMat);
            var indexer = typeSpecificMat.GetIndexer();

            var (item0, item1, item2) = indexer[searchPoint.Y, searchPoint.X];

            return Color.FromArgb(item2, item1, item0);
        }

        /// <summary>
        ///     Searches for a bitmap within the backbuffer that matches a given confidence
        /// </summary>
        /// <param name="needle">The image to search for</param>
        /// <param name="confidence">
        ///     The minimum confidence to return. If this is set to 0 it will return the highest confidence value
        /// </param>
        /// <returns>The found area and confidence that was found</returns>
        public RectWithConfidence? FindMatch(Bitmap needle, double confidence = .8)
        {
            using var result = new Mat();

            Cv2.MatchTemplate(needle.ToMat(), ScreenshotMat, result, TemplateMatchModes.CCoeffNormed);
            Cv2.MinMaxLoc(result, out var minVal, out _, out var minLoc, out _);

            var threshold = 1 - minVal;
            return confidence < threshold
                ? new RectWithConfidence(new Rect(minLoc, needle.Size.ToOpenCvSize()), threshold)
                : null;
        }

        /// <summary>
        ///     Searches for a bitmap within the backbuffer that matches a given confidence
        /// </summary>
        /// <param name="needle">The image to search for</param>
        /// <param name="confidence">
        ///     The minimum confidence to return. If this is set to 0 it will return the highest confidence value
        /// </param>
        /// <returns>The found areas and confidences that were found</returns>
        public IEnumerable<RectWithConfidence> FindMatches(Bitmap needle, double confidence = .8)
        {
            var rects = new List<RectWithConfidence>();
            using var result = new Mat();

            Cv2.MatchTemplate(needle.ToMat(), ScreenshotMat, result, TemplateMatchModes.CCoeffNormed);
            Cv2.Threshold(result, result, confidence, 1, ThresholdTypes.Tozero);

            Cv2.MinMaxLoc(result, out _, out var maxVal, out _, out var maxLoc);

            while (maxVal >= confidence)
            {
                //Setup the rectangle to draw
                var r = new Rect(new OpenCvSharp.Point(maxLoc.X, maxLoc.Y),
                    new Size(needle.Width, needle.Height));

                //Fill in the res Mat so you don't find the same area again in the MinMaxLoc
                Cv2.FloodFill(result, maxLoc, new Scalar(0), out _, new Scalar(.1), new Scalar(1));

                rects.Add(new RectWithConfidence(r, maxVal));

                Cv2.MinMaxLoc(result, out _, out maxVal, out _, out maxLoc);
            }

            return rects;
        }
    }
}