﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;

namespace AutoCV
{
    // ReSharper disable InconsistentNaming
    public static class Win32Wrapper
    {
        public delegate bool EnumTopWindows(IntPtr hwnd, IntPtr lParam);

        public delegate bool EnumWindowProc(IntPtr hwnd, IntPtr lParam);

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum Constants
        {
            WM_KEYDOWN = 256,
            WM_KEYUP = 257,
            WM_CHAR = 258,
            WM_LBUTTONDOWN = 0x201,
            WM_LBUTTONUP = 0x202,
            WM_LBUTTONDBLCLK = 0x203,
            WM_RBUTTONDOWN = 0x204,
            WM_RBUTTONUP = 0x205,
            WM_RBUTTONDBLCLK = 0x206,
            MK_LBUTTON = 0x0001,
            WM_MOUSEMOVE = 0x0200,
            VK_RETURN = 0x0D
        }

        // ReSharper disable once InconsistentNaming
        public static int MAKELPARAM(int p, int p2)
        {
            return (p2 << 16) | (p & 0xFFFF);
        }

        public static bool GetWindowRect(IntPtr hWnd, out Rect lpRect)
        {
            return User32.GetWindowRect(hWnd, out lpRect);
        }

        public static bool PrintWindow(IntPtr hWnd, IntPtr hdcBlt, int nFlags)
        {
            return User32.PrintWindow(hWnd, hdcBlt, nFlags);
        }

        public static IntPtr GetWindowDC(IntPtr hWnd)
        {
            return User32.GetWindowDC(hWnd);
        }

        // ReSharper disable once InconsistentNaming
        public static IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDC)
        {
            return User32.ReleaseDC(hWnd, hDC);
        }

        public static bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr lParam)
        {
            return User32.EnumChildWindows(window, callback, lParam);
        }

        public static int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount)
        {
            return User32.GetClassName(hWnd, lpClassName, nMaxCount);
        }

        public static int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount)
        {
            return User32.GetWindowText(hWnd, lpString, nMaxCount);
        }

        public static int EnumWindows(EnumTopWindows callPtr, IntPtr lPar)
        {
            return User32.EnumWindows(callPtr, lPar);
        }

        public static bool IsWindowVisible(IntPtr hWnd)
        {
            return User32.IsWindowVisible(hWnd);
        }

        public static bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk)
        {
            return User32.RegisterHotKey(hWnd, id, fsModifiers, vk);
        }

        public static bool UnregisterHotKey(IntPtr hWnd, int id)
        {
            return User32.UnregisterHotKey(hWnd, id);
        }

        public static bool PostMessage(IntPtr hWnd, uint msg, int wParam, uint lParam)
        {
            return User32.PostMessage(hWnd, msg, wParam, lParam);
        }

        public static short VkKeyScan(char ch)
        {
            return User32.VkKeyScan(ch);
        }

        public static IntPtr SendMessageA(IntPtr hWnd, uint wMsg, int wParam, uint lParam)
        {
            return User32.SendMessageA(hWnd, wMsg, wParam, lParam);
        }

        public static bool ScreenToClient(IntPtr hWnd, ref POINT lpPoint)
        {
            return User32.ScreenToClient(hWnd, ref lpPoint);
        }

        [StructLayout(LayoutKind.Sequential)]
        public readonly struct POINT
        {
            public readonly int X;
            public readonly int Y;

            public POINT(int x, int y)
            {
                X = x;
                Y = y;
            }

            public static implicit operator Point(POINT p)
            {
                return new Point(p.X, p.Y);
            }

            public static implicit operator POINT(Point p)
            {
                return new POINT(p.X, p.Y);
            }
        }


        [StructLayout(LayoutKind.Sequential)]
        public readonly struct Rect
        {
            public int Left { get; }
            public int Top { get; }
            public int Right { get; }
            public int Bottom { get; }
            public int Width => Right - Left;
            public int Height => Bottom - Top;
            public Point Location => new(Left, Top);
            public Size Size => new(Width, Height);

            public Rect(int left, int top, int right, int bottom)
            {
                Left = left;
                Top = top;
                Right = right;
                Bottom = bottom;
            }
        }
    }
}