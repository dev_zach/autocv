﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using AutoCV.CustomExceptions;
using AutoCV.Extensions;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using static System.String;
using static AutoCV.User32;
using Point = OpenCvSharp.Point;

namespace AutoCV
{
    public class WindowCapture : IDisposable
    {
        private Bitmap _screenshot = null!;
        public IntPtr Handle { get; set; }
        private protected Mat ScreenshotMat { get; private set; } = null!;

        protected Bitmap Screenshot
        {
            get => _screenshot;
            private protected set
            {
                _screenshot = value;
                ScreenshotMat = value.ToMat();
            }
        }

        private Rectangle WindowRect { get; set; }

        public virtual void Dispose()
        {
            GC.SuppressFinalize(this);
            _screenshot.Dispose();
            ScreenshotMat.Dispose();
        }

        /// <summary>
        ///     Erases a portion of the backbuffer
        /// </summary>
        /// <param name="rect">the area of the backbuffer to erase</param>
        public void EraseBackbuffer(Rect rect = default)
        {
            if (rect == default)
                rect = new Rect(new Point(0, 0), ScreenshotMat.Size());

            using var subMat = ScreenshotMat.SubMat(rect);
            subMat.SetTo(0);
        }

        /// <summary>
        ///     Refreshes the backbuffer
        /// </summary>
        /// <exception cref="WindowNotSetException"></exception>
        public void Refresh()
        {
            if (Handle == default)
                throw new WindowNotSetException("Please set a handle before calling this function.");

            var hdcSrc = GetWindowDC(Handle);
            GetWindowRect(Handle, out var rect);
            WindowRect = new Rectangle(rect.Location, rect.Size);

            var hdcDest = Gdi32Wrapper.CreateCompatibleDC(hdcSrc);
            var hBitmap = Gdi32Wrapper.CreateCompatibleBitmap(hdcSrc, WindowRect.Width, WindowRect.Height);
            var hOld = Gdi32Wrapper.SelectObject(hdcDest, hBitmap);

            Gdi32Wrapper.BitBlt(hdcDest, 0, 0, WindowRect.Width, WindowRect.Height, hdcSrc, 0, 0, Gdi32Wrapper.SRCCOPY);
            Gdi32Wrapper.SelectObject(hdcDest, hOld);
            Gdi32Wrapper.DeleteDC(hdcDest);
            ReleaseDC(Handle, hdcSrc);

            if (hBitmap == IntPtr.Zero) return;

            var screenshot = Image.FromHbitmap(hBitmap);
            Gdi32Wrapper.DeleteObject(hBitmap);
            Screenshot = screenshot;
        }

        /// <summary>
        ///     Sets the backbuffer hwnd
        /// </summary>
        /// <param name="handle">the handle of the window to set</param>
        /// <returns>true if the window was found else false</returns>
        public bool SetHwnd(IntPtr handle)
        {
            var topHandles = GetTopHandles();
            if (topHandles.Contains(handle))
            {
                Handle = handle;
                return true;
            }

            if (Handle == default) return false;

            var childHandles = GetChildHandles();
            if (!childHandles.Contains(handle)) return false;

            Handle = handle;
            return true;
        }

        /// <summary>
        ///     Sets the backbuffer hwnd
        /// </summary>
        /// <param name="windowName">The name of the window to set</param>
        /// <returns>true if the windows was found else false</returns>
        public bool SetHwnd(string windowName)
        {
            var parentHandles = GetTopHandlesWithName();
            var windowHandle = parentHandles.FirstOrDefault(x => x.Value.ContainsIgnoreCase(windowName)).Key;

            // if the parent handle couldn't be found
            if (windowHandle != default)
            {
                Handle = windowHandle;
                return true;
            }


            // can't find the child windows of a null parent handle
            if (Handle == default) return false;

            var childHandles = GetChildHandlesWithTitle();
            windowHandle = childHandles.FirstOrDefault(x => x.Value == windowName).Key;

            if (windowHandle == default) return false;

            Handle = windowHandle;
            return true;
        }

        /// <summary>
        ///     Sets the backbuffer hwnd
        /// </summary>
        /// <param name="handle">the handle to set</param>
        /// <returns>true if the hwnd was set else false</returns>
        public bool SetHwnd(int handle)
        {
            return SetHwnd((IntPtr)handle);
        }

        /// <summary>
        ///     Gets the child handles with titles
        /// </summary>
        /// <returns></returns>
        public Dictionary<IntPtr, string> GetChildHandlesWithTitle()
        {
            var childHandles = GetChildHandles();

            var namedHandles = new Dictionary<IntPtr, string>();
            foreach (var childHandle in childHandles)
            {
                var handleName = GetClassNameByHwnd(childHandle);
                namedHandles.Add(childHandle, handleName);
            }

            return namedHandles;
        }

        public IEnumerable<IntPtr> GetChildHandles()
        {
            var childHandles = new List<IntPtr>();

            var gcChildhandlesList = GCHandle.Alloc(childHandles);
            var pointerChildHandlesList = GCHandle.ToIntPtr(gcChildhandlesList);

            try
            {
                var childProc = new Win32Wrapper.EnumWindowProc(EnumWindow);
                EnumChildWindows(Handle, childProc, pointerChildHandlesList);
            }
            finally
            {
                gcChildhandlesList.Free();
            }

            return childHandles;
        }

        public string GetClassNameByHwnd(IntPtr handle)
        {
            var handleClassName = new StringBuilder(256);
            GetClassName(handle, handleClassName, handleClassName.Capacity);

            return handleClassName.ToString();
        }

        public Dictionary<IntPtr, string> GetTopHandlesWithName()
        {
            var topHandles = GetTopHandles();

            var namedHandles = new Dictionary<IntPtr, string>();
            foreach (var topHandle in topHandles)
            {
                var handleName = GetWindowTextByHwnd(topHandle);
                if (!IsNullOrEmpty(handleName))
                    namedHandles.Add(topHandle, handleName);
            }

            return namedHandles;
        }

        private static IEnumerable<IntPtr> GetTopHandles()
        {
            var handles = new List<IntPtr>();
            var listgc = GCHandle.Alloc(handles);

            try
            {
                Win32Wrapper.EnumTopWindows callbackPtr = GetTopHandle;
                EnumWindows(callbackPtr, GCHandle.ToIntPtr(listgc));
            }
            finally
            {
                listgc.Free();
            }

            return handles.Where(IsWindowVisible).ToList();
        }

        private static bool GetTopHandle(IntPtr handle, IntPtr lParam)
        {
            var gcChildhandlesList = GCHandle.FromIntPtr(lParam);

            if (gcChildhandlesList.Target == null) return false;

            var childHandles = gcChildhandlesList.Target as List<IntPtr>;
            childHandles?.Add(handle);
            return true;
        }

        public string GetWindowTextByHwnd(IntPtr handle = default)
        {
            StringBuilder handleWindowText = new(256);
            GetWindowText(handle == default ? Handle : handle, handleWindowText, handleWindowText.Capacity);

            return handleWindowText.ToString();
        }


        private static bool EnumWindow(IntPtr hWnd, IntPtr lParam)
        {
            var gcChildhandlesList = GCHandle.FromIntPtr(lParam);

            if (gcChildhandlesList.Target == null) return false;

            var childHandles = gcChildhandlesList.Target as List<IntPtr>;
            childHandles?.Add(hWnd);

            return true;
        }
    }
}