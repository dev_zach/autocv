﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace BitmapPicker
{
    public class BitmapPickerEventArgs : EventArgs
    {
        public BitmapPickerEventArgs(Bitmap bmp)
        {
            SelectedBitmap = bmp;
        }

        public Bitmap SelectedBitmap { get; }
    }

    public class BitmapPicker
    {
        public delegate void BitmapSelected(object sender, BitmapPickerEventArgs e);

        private FrmSnipBuffer _picker;
        public event BitmapSelected OnBitmapSelected;

        public void Show(BitmapPickerReturnType returnType)
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();

            var t = new Thread(() =>
            {
                _picker = new FrmSnipBuffer(returnType);
                _picker.OnBitmapSelected += Picker_OnBitmapSelected;
                Application.Run(_picker);
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        private void Picker_OnBitmapSelected(object sender, BitmapPickerEventArgs e)
        {
            OnBitmapSelected?.Invoke(sender, e);
        }
    }
}