﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using AutoCV.Helpers;

namespace BitmapPicker
{
    public enum BitmapPickerReturnType
    {
        String,
        Bitmap,
        OpenImageViewer,
        BitmapAndOpenImageViewer
    }

    public partial class FrmSnipBuffer : Form
    {
        public delegate void BitmapSelected(object sender, BitmapPickerEventArgs e);

        private readonly BitmapPickerReturnType _returnBehaviour;

        private bool _dragging;
        private Point _initPt;
        private Rectangle _selection;

        public FrmSnipBuffer(BitmapPickerReturnType returnType)
        {
            InitializeComponent();
            var screenshot = Utils.RegularScreenshot();
            pbBitmap.Image = screenshot;

            _returnBehaviour = returnType;

            pbBitmap.MouseDown += PbBitmap_MouseDown;
            pbBitmap.MouseMove += PbBitmap_MouseMove;
            pbBitmap.MouseUp += PbBitmap_MouseUp;
            pbBitmap.Paint += PbBitmap_Paint;
            KeyDown += FrmSnipBuffer_KeyDown;
            FormClosing += FrmSnipBuffer_FormClosing;
        }

        public event BitmapSelected OnBitmapSelected;

        private void FrmSnipBuffer_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void FrmSnipBuffer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) Close();
        }

        private void PbBitmap_Paint(object sender, PaintEventArgs e)
        {
            if (!_dragging) return;
            var g = e.Graphics;
            var origin = _initPt;
            var localOrigin = pbBitmap.PointToClient(origin);
            var localCursor = pbBitmap.PointToClient(Cursor.Position);

            var x = Math.Min(localCursor.X, localOrigin.X);
            var y = Math.Min(localCursor.Y, localOrigin.Y);

            var w = Math.Max(localCursor.X, localOrigin.X) - x;
            var h = Math.Max(localCursor.Y, localOrigin.Y) - y;

            var rect = new Rectangle(x, y, w, h);
            var pen = new Pen(Color.White) { DashCap = DashCap.Round, DashPattern = new[] { 2.0f, 2.0f } };

            g.FillRectangle(new SolidBrush(Color.FromArgb(50, Color.Blue)), rect);
            g.DrawRectangle(pen, rect);

            _selection = rect;
        }

        private void PbBitmap_MouseUp(object sender, MouseEventArgs e)
        {
            _dragging = false;
            _initPt = default;

            var fullBmp = (Bitmap)pbBitmap.Image;
            var snipBmp = Utils.GetBitmapRegion(fullBmp, _selection);

            if (_returnBehaviour is BitmapPickerReturnType.OpenImageViewer or BitmapPickerReturnType
                .BitmapAndOpenImageViewer)
            {
                var viewer = new ImageViewer.ImageViewer(snipBmp);
            }

            OnBitmapSelected?.Invoke(this, new BitmapPickerEventArgs(snipBmp));

            // Need to throw event that then closes this form, use colour picker as an example
            // this.Close();
        }

        private void PbBitmap_MouseMove(object sender, MouseEventArgs e)
        {
            if (_dragging) pbBitmap.Invalidate();
        }

        private void PbBitmap_MouseDown(object sender, MouseEventArgs e)
        {
            _dragging = true;
            _initPt = Cursor.Position;
        }
    }
}