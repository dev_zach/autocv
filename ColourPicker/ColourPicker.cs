﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace ClrPicker
{
    /// <summary>
    ///     Event arguments that allow passing colour through the colour selected event
    /// </summary>
    public class PickerEventArgs : EventArgs
    {
        public PickerEventArgs(Color colour)
        {
            SelectedColour = colour;
        }

        public Color SelectedColour { get; }
    }

    /// <summary>
    ///     ColourPicker management class
    /// </summary>
    public class ColourPicker
    {
        public delegate void ColourSelected(object sender, PickerEventArgs e);

        private FrmMain _picker;

        public event ColourSelected OnColourSelected;

        /// <summary>
        ///     Show the colour picker (launches in a separate STA thread).
        /// </summary>
        /// <param name="hwnd">The ColourPicker will only work on this hwnd</param>
        public void Show(IntPtr hwnd)
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();

            var t = new Thread(() =>
            {
                _picker = new FrmMain(hwnd);
                _picker.OnColourSelected += Picker_OnColourSelected;
                Application.Run(_picker);
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        public void Show(string hwnd)
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();

            var t = new Thread(() =>
            {
                _picker = new FrmMain(hwnd);
                _picker.OnColourSelected += Picker_OnColourSelected;
                Application.Run(_picker);
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        private void Picker_OnColourSelected(object sender, PickerEventArgs e)
        {
            OnColourSelected?.Invoke(sender, e);
        }
    }
}