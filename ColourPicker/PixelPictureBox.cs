﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace ClrPicker
{
    /// <summary>
    ///     Basically just a picture box but with a hacky grid and crosshair (not DPI friendly)
    /// </summary>
    public partial class PixelPictureBox : PictureBox
    {
        private int _cellCount;
        private float _cellSize;


        public PixelPictureBox()
        {
            InitializeComponent();
        }

        public PixelPictureBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <value>Set this to Nearest Neighbour to avoid quality loss on zoom</value>
        public InterpolationMode InterpolationMode { get; set; }

        /// <value>Show the grid</value>
        public bool ShowGrid { get; set; } = true;

        /// <value>How many wide/high are each grid "pixel"</value>
        public float GridSize { get; set; } = 7;

        /// <value>Returns colour of the pixel in the middle of the crosshair.</value>
        public Color CentreColour
        {
            get
            {
                var bmp = new Bitmap(Image);
                return bmp.GetPixel(bmp.Width / 2, bmp.Height / 2);
            }
        }

        [SuppressMessage("ReSharper", "PossibleLossOfFraction")]
        protected override void OnPaint(PaintEventArgs paintEventArgs)
        {
            paintEventArgs.Graphics.PixelOffsetMode = PixelOffsetMode.Half;
            paintEventArgs.Graphics.InterpolationMode = InterpolationMode;
            base.OnPaint(paintEventArgs);

            _cellCount = (int)(GridSize * GridSize);
            _cellSize = Width / GridSize;

            if (!ShowGrid || _cellCount <= 0 || !(_cellSize > 0)) return;

            var g = paintEventArgs.Graphics;
            var gridPen = new Pen(Color.Black);

            // Crosshair
            var crosshairColour = Color.Fuchsia;
            g.FillRectangle(new SolidBrush(crosshairColour),
                new RectangleF(Width / 2 - _cellSize / 2, 0, _cellSize, Height / 2 - _cellSize / 2));
            g.FillRectangle(new SolidBrush(crosshairColour),
                new RectangleF(Width / 2 - _cellSize / 2, Height / 2 + _cellSize / 2, _cellSize,
                    Height / 2 - _cellSize / 2));
            g.FillRectangle(new SolidBrush(crosshairColour),
                new RectangleF(0, Height / 2 - _cellSize / 2, Width / 2 - _cellSize / 2, _cellSize));
            g.FillRectangle(new SolidBrush(crosshairColour),
                new RectangleF(Width / 2 + _cellSize / 2, Height / 2 - _cellSize / 2, Width / 2 - _cellSize / 2,
                    _cellSize));

            // Grid
            for (var x = 0; x < _cellCount; x++)
                g.DrawLine(gridPen, x * _cellSize, 0, x * _cellSize, _cellCount * _cellSize);

            for (var y = 0; y < _cellCount; y++)
                g.DrawLine(gridPen, 0, y * _cellSize, _cellCount * _cellSize, y * _cellSize);
        }
    }
}