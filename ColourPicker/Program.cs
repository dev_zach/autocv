using System;
using System.Windows.Forms;

namespace ClrPicker
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            // Need DPI awareness for mouse shit to work right
            Application.SetHighDpiMode(HighDpiMode.PerMonitorV2);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmMain());
        }
    }
}