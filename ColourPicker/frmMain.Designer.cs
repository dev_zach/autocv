﻿
namespace ClrPicker
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmrMouse = new System.Windows.Forms.Timer(this.components);
            this.pbColour = new ClrPicker.PixelPictureBox(this.components);
            this.lblHotkey = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbColour)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrMouse
            // 
            this.tmrMouse.Enabled = true;
            this.tmrMouse.Interval = 15;
            // 
            // pbColour
            // 
            this.pbColour.BackColor = System.Drawing.Color.White;
            this.pbColour.GridSize = 7F;
            this.pbColour.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            this.pbColour.Location = new System.Drawing.Point(0, 0);
            this.pbColour.Name = "pbColour";
            this.pbColour.ShowGrid = true;
            this.pbColour.Size = new System.Drawing.Size(58, 63);
            this.pbColour.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbColour.TabIndex = 4;
            this.pbColour.TabStop = false;
            // 
            // lblHotkey
            // 
            this.lblHotkey.AutoSize = true;
            this.lblHotkey.BackColor = System.Drawing.Color.Transparent;
            this.lblHotkey.ForeColor = System.Drawing.Color.White;
            this.lblHotkey.Location = new System.Drawing.Point(-3, 77);
            this.lblHotkey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHotkey.Name = "lblHotkey";
            this.lblHotkey.Size = new System.Drawing.Size(78, 50);
            this.lblHotkey.TabIndex = 5;
            this.lblHotkey.Text = "Capture:\r\nAlt+C";
            this.lblHotkey.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(77, 142);
            this.Controls.Add(this.lblHotkey);
            this.Controls.Add(this.pbColour);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1, 2);
            this.Name = "FrmMain";
            this.ShowInTaskbar = false;
            this.Text = "AutoCV Colour Picker";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pbColour)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmrMouse;
        private PixelPictureBox pbColour;
        private System.Windows.Forms.Label lblHotkey;
    }
}

