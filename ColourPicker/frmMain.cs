﻿using System;
using System.Drawing;
using System.Windows.Forms;
using AutoCV;
using AutoCV.Helpers;

namespace ClrPicker
{
    /// <summary>
    ///     ColourPicker form
    /// </summary>
    public partial class FrmMain : Form
    {
        /// <value>This event gets raised when a colour is selected using the hotkey</value>
        public delegate void ColourSelected(object sender, PickerEventArgs e);

        private const int GridSize = 7;

        /// <value>This AutoCV instance is used mostly just to capture WindowRect and Hwnd</value>
        private readonly AutoCV.AutoCV _cv;

        private bool _hasHwnd = true;
        private KeyboardHook _hook;

        public FrmMain(string hwndText = "")
        {
            InitializeComponent();
            _cv = new AutoCV.AutoCV();
            _cv.SetHwnd(hwndText ?? "paint");
            Setup();
        }

        public FrmMain(IntPtr hwnd)
        {
            InitializeComponent();
            _cv = new AutoCV.AutoCV();
            _cv.SetHwnd(hwnd);
            Setup();
        }

        /// <value>This event gets raised when a colour is selected using the hotkey</value>
        public event ColourSelected OnColourSelected;

        public void Setup()
        {
            tmrMouse.Tick += TmrMouse_Tick;

            // Register capture hotkey as Alt+C
            _hook = new KeyboardHook();
            _hook.KeyPressed += Hook_KeyPressed;
            _hook.RegisterHotKey(global::ModifierKeys.Alt, Keys.C);

            // Dumb DPI scaling hack that basically reshapes the whole form based on gridsize, which is 7
            var viewerSize = Width;
            while (viewerSize % pbColour.GridSize > 0 && viewerSize > 0) viewerSize -= 1;

            Width = viewerSize;
            Height = viewerSize + lblHotkey.Height;
            lblHotkey.Location = new Point(0, viewerSize);
            lblHotkey.Width = viewerSize;

            pbColour.Width = viewerSize;
            pbColour.Height = viewerSize;
        }

        private void Hook_KeyPressed(object sender, KeyPressedEventArgs e)
        {
            if (_hasHwnd && OnColourSelected != null)
                OnColourSelected(this, new PickerEventArgs(pbColour.CentreColour));
            Application.DoEvents();
            tmrMouse.Enabled = false;
            Close();
        }

        private void TmrMouse_Tick(object sender, EventArgs e)
        {
            Left = Cursor.Position.X + 20;
            Top = Cursor.Position.Y + 20;

            try
            {
                _cv.Refresh();
            }
            catch (Exception)
            {
                _hasHwnd = false;
                BackColor = Color.Red;
                return;
            }

            _hasHwnd = true;

            Win32Wrapper.POINT uPoint = Cursor.Position;
            Win32Wrapper.ScreenToClient(_cv.Handle, ref uPoint);

            if (uPoint.X > 0 && uPoint.Y > 0) BackColor = Color.Green;
            else BackColor = Color.Red;

            pbColour.Image =
                Utils.GetPixelGrid(_cv.Handle, new Point(uPoint.X, uPoint.Y), GridSize, GridSize);
        }
    }
}