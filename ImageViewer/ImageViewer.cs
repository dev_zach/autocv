using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace ImageViewer
{
    /// <summary>
    ///     Manager class for the ImageViewer form. Show image viewer using .ShowBackBuffer()
    /// </summary>
    public class ImageViewer
    {
        private FrmMain _viewer;

        public ImageViewer(Image img = null)
        {
            if (img != null) NewForm(img);
        }

        // Kill form instance when it's closed
        private void Viewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            _viewer = null;
        }

        // If no viewer is open, make a new viewer before setting the image
        public void SetImage(Image img)
        {
            if (_viewer == null)
                NewForm(img);
            else
                _viewer.SetImage(img);
        }

        public void SetLocation(Point pt)
        {
            if (_viewer == null)
                throw new Exception("Can't change location of viewer, since viewer has been destroyed.");
            _viewer.SetLocation(pt);
        }

        // To avoid freezing the console we need to start the viewer on a new STA thread
        private void NewForm(Image img)
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            var frozenImage = new Bitmap(img);

            var t = new Thread(() =>
            {
                _viewer = new FrmMain(frozenImage, true);
                _viewer.FormClosing += Viewer_FormClosing;
                Application.Run(_viewer);
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }
    }
}