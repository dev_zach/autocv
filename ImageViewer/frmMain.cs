﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ImageViewer
{
    /// <summary>
    ///     Form for the image viewer. Accessed using AutoCV's .ShowBackBuffer()
    /// </summary>
    internal partial class FrmMain : Form
    {
        public FrmMain(Image img, bool autoSize)
        {
            InitializeComponent();
            if (img == null || !autoSize) return;

            pbMain.Image = img;
            // If image is larger than the screen, start viewer maximized, otherwise make it the same size as the image
            var screenRect = Screen.GetBounds(Location);
            if (img.Size.Width >= screenRect.Width || img.Size.Height >= screenRect.Height)
                WindowState = FormWindowState.Maximized;
            else
                ClientSize = new Size(img.Size.Width, img.Size.Height + panTools.Height);
        }

        public void SetImage(Image img)
        {
            // Have to use invoking because we're setting the image from a separate thread
            if (pbMain.InvokeRequired)
                pbMain.Invoke(new Action(() =>
                {
                    pbMain.Image = img;
                    pbMain.Invalidate();
                    pbMain.Update();
                }));
        }

        public void SetLocation(Point loc)
        {
            // Have to use invoking because we're setting the location from a separate thread
            if (InvokeRequired)
                Invoke(new Action(() => { Location = loc; }));
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var sfd = new SaveFileDialog
            {
                Filter = @"Bitmap Files (*.bmp)|*.bmp|PNG Files (*.png)|*.png|Other (*.*)|*.*"
            };
            if (sfd.ShowDialog() == DialogResult.Cancel) return;
            pbMain.Image.Save(sfd.FileName);
        }
    }
}