﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using AutoCV;
using OpenCvSharp;
using Point = System.Drawing.Point;

namespace main
{
    internal static class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            const string path = "USER32.dll";
            var process = Process.GetProcessesByName("OpenOSRS").First();

            var processModule = process.Modules.Cast<ProcessModule>()
                .First(m => m.ModuleName?.ToLower() == path.ToLower());

            var baseAddress = processModule.BaseAddress;
            var offset = GetProcAddress(baseAddress, "GetCursorPos").ToInt64() - baseAddress.ToInt64();
            if (process.MainModule != null)
            {
                var function = new IntPtr(process.MainModule.BaseAddress.ToInt64() + offset);

                byte[] byteArray = { 0xB0, 0x00, 0x84, 0xC0, 0xC3 };
                WriteProcessMemory(process.Handle, function, byteArray, byteArray.Length, out _);
            }

            Login();
        }

        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("kernel32.dll")]
        private static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int nSize,
            out IntPtr lpNumberOfBytesWritten);

        public static void Login()
        {
            using var autocv = new AutoCV.AutoCV();
            autocv.SetHwnd("OpenOSRS");
            while (autocv.SetHwnd("SunAwtCanvas"))
            {
            }

            autocv.Refresh();
            var existingUserButton = autocv.FindText("Existing User");
            if (existingUserButton.HasValue)
                autocv.ClickMouse(RandomPointInRect(existingUserButton.Value.ComponentRect.ToOpenCvRect()));
        }

        private static Point RandomPointInRect(Rect rect)
        {
            var random = new Random();
            var point = new Point(random.Next(rect.Width) + rect.Left, random.Next(rect.Height) + rect.Top);
            return point;
            // 472, 295
        }
    }
}