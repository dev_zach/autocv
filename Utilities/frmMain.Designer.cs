﻿
namespace Utilities
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblScreenCoords = new System.Windows.Forms.Label();
            this.tmrCoords = new System.Windows.Forms.Timer(this.components);
            this.txtHwnd = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblClientCoords = new System.Windows.Forms.Label();
            this.btnApplyHwnd = new System.Windows.Forms.Button();
            this.btnColourPicker = new System.Windows.Forms.Button();
            this.pbColour = new System.Windows.Forms.PictureBox();
            this.txtColour = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbColour)).BeginInit();
            this.SuspendLayout();
            // 
            // lblScreenCoords
            // 
            this.lblScreenCoords.AutoSize = true;
            this.lblScreenCoords.Location = new System.Drawing.Point(8, 70);
            this.lblScreenCoords.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblScreenCoords.Name = "lblScreenCoords";
            this.lblScreenCoords.Size = new System.Drawing.Size(110, 15);
            this.lblScreenCoords.TabIndex = 0;
            this.lblScreenCoords.Text = "Screen coordinates:";
            // 
            // tmrCoords
            // 
            this.tmrCoords.Enabled = true;
            this.tmrCoords.Interval = 10;
            // 
            // txtHwnd
            // 
            this.txtHwnd.Location = new System.Drawing.Point(57, 101);
            this.txtHwnd.Margin = new System.Windows.Forms.Padding(2);
            this.txtHwnd.Name = "txtHwnd";
            this.txtHwnd.Size = new System.Drawing.Size(106, 23);
            this.txtHwnd.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 103);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Hwnd:";
            // 
            // lblClientCoords
            // 
            this.lblClientCoords.AutoSize = true;
            this.lblClientCoords.Location = new System.Drawing.Point(8, 136);
            this.lblClientCoords.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblClientCoords.Name = "lblClientCoords";
            this.lblClientCoords.Size = new System.Drawing.Size(106, 15);
            this.lblClientCoords.TabIndex = 3;
            this.lblClientCoords.Text = "Client coordinates:";
            // 
            // btnApplyHwnd
            // 
            this.btnApplyHwnd.Location = new System.Drawing.Point(166, 100);
            this.btnApplyHwnd.Margin = new System.Windows.Forms.Padding(2);
            this.btnApplyHwnd.Name = "btnApplyHwnd";
            this.btnApplyHwnd.Size = new System.Drawing.Size(78, 20);
            this.btnApplyHwnd.TabIndex = 4;
            this.btnApplyHwnd.Text = "Apply";
            this.btnApplyHwnd.UseVisualStyleBackColor = true;
            this.btnApplyHwnd.Click += new System.EventHandler(this.btnApplyHwnd_Click);
            // 
            // btnColourPicker
            // 
            this.btnColourPicker.Enabled = false;
            this.btnColourPicker.Location = new System.Drawing.Point(8, 211);
            this.btnColourPicker.Margin = new System.Windows.Forms.Padding(2);
            this.btnColourPicker.Name = "btnColourPicker";
            this.btnColourPicker.Size = new System.Drawing.Size(132, 20);
            this.btnColourPicker.TabIndex = 5;
            this.btnColourPicker.Text = "Colour Picker";
            this.btnColourPicker.UseVisualStyleBackColor = true;
            this.btnColourPicker.Click += new System.EventHandler(this.btnColourPicker_Click);
            // 
            // pbColour
            // 
            this.pbColour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbColour.Location = new System.Drawing.Point(8, 236);
            this.pbColour.Name = "pbColour";
            this.pbColour.Size = new System.Drawing.Size(62, 54);
            this.pbColour.TabIndex = 6;
            this.pbColour.TabStop = false;
            // 
            // txtColour
            // 
            this.txtColour.Location = new System.Drawing.Point(76, 236);
            this.txtColour.Multiline = true;
            this.txtColour.Name = "txtColour";
            this.txtColour.ReadOnly = true;
            this.txtColour.Size = new System.Drawing.Size(180, 76);
            this.txtColour.TabIndex = 7;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 412);
            this.Controls.Add(this.txtColour);
            this.Controls.Add(this.pbColour);
            this.Controls.Add(this.btnColourPicker);
            this.Controls.Add(this.btnApplyHwnd);
            this.Controls.Add(this.lblClientCoords);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtHwnd);
            this.Controls.Add(this.lblScreenCoords);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmMain";
            this.Text = "AutoCV Utilities";
            ((System.ComponentModel.ISupportInitialize)(this.pbColour)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblScreenCoords;
        private System.Windows.Forms.Timer tmrCoords;
        private System.Windows.Forms.TextBox txtHwnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblClientCoords;
        private System.Windows.Forms.Button btnApplyHwnd;
        private System.Windows.Forms.Button btnColourPicker;
        private System.Windows.Forms.PictureBox pbColour;
        private System.Windows.Forms.TextBox txtColour;
    }
}

