﻿using System;
using System.Drawing;
using System.Windows.Forms;
using AutoCV;
using ClrPicker;

namespace Utilities
{
    public partial class FrmMain : Form
    {
        private IntPtr _appliedHwnd = IntPtr.Zero;

        public FrmMain()
        {
            InitializeComponent();
            tmrCoords.Tick += TmrCoords_Tick;
        }

        private void TmrCoords_Tick(object sender, EventArgs e)
        {
            lblScreenCoords.Text = @$"Screen coordinates: {Cursor.Position}";
            if (_appliedHwnd == IntPtr.Zero) return;

            Win32Wrapper.POINT pt = Cursor.Position;
            Win32Wrapper.ScreenToClient(_appliedHwnd, ref pt);
            var newPt = new Point(pt.X, pt.Y);
            lblClientCoords.Text = @$"Client coordinates: {newPt}";
        }

        private void btnApplyHwnd_Click(object sender, EventArgs e)
        {
            _appliedHwnd = new IntPtr(Convert.ToInt32(txtHwnd.Text));
            btnColourPicker.Enabled = _appliedHwnd != IntPtr.Zero;
        }

        private void btnColourPicker_Click(object sender, EventArgs e)
        {
            var cp = new ColourPicker();
            cp.Show(_appliedHwnd);
            cp.OnColourSelected += Cp_OnColourSelected;
        }

        private void Cp_OnColourSelected(object sender, PickerEventArgs e)
        {
            pbColour.BackColor = e.SelectedColour;
            if (txtColour.InvokeRequired)
                txtColour.Invoke(new Action(() =>
                {
                    var knownColour = e.SelectedColour.ToKnownColor().ToString();
                    if (knownColour == "0" || knownColour.StartsWith("0"))
                        knownColour = "No name for this colour.";
                    txtColour.Text += knownColour + Environment.NewLine;
                    txtColour.Text += HexConverter(e.SelectedColour) + Environment.NewLine;
                    txtColour.Text += RgbConverter(e.SelectedColour) + Environment.NewLine;
                }));
        }

        private static string HexConverter(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        private static string RgbConverter(Color c)
        {
            return $@"RGB({c.R}, {c.G}, {c.B})";
        }
    }
}